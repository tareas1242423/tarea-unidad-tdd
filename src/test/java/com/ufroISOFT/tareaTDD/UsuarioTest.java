package com.ufroISOFT.tareaTDD;

import com.ufroISOFT.tareaTDD.services.IUsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UsuarioTest {

    @Autowired
    private IUsuarioService usuarioService;

    @DisplayName("ingresar nombre mal ")
    @ParameterizedTest
    @ValueSource(strings = {"G4br13L" , "An61dy" , "N1co" , "123Ga3bri3e3l31231"})
    void nombreNoEstaCorrectoTest(String nombre){
        boolean resultado = usuarioService.validarNombre(nombre);
        Assertions.assertFalse(resultado);

    }

    @DisplayName("ingresar nombre bien ")
    @ParameterizedTest
    @ValueSource(strings = {"Gabriel" , "Andy" , "Nico" , "Daniel"})
    void nombreEstaCorrectoTest(String nombre){
        boolean resultado = usuarioService.validarNombre(nombre);
        Assertions.assertTrue(resultado);
    }

    @DisplayName("Validar un rut malo ")
    @ParameterizedTest
    @ValueSource(strings = {"20643527-1" , "20643723-1" , "23402241-k" , "123k1231232"})
    void rutNoEstaCorrectoTest(String rut){
        boolean resultado = usuarioService.validarRut(rut);
        Assertions.assertFalse(resultado);
    }

    @DisplayName("Validar un rut bueno ")
    @ParameterizedTest
    @ValueSource(strings = {"20643527-5" , "20.643.527-5" , "17766152-k" , "17.652.660-2" , "18.436.486-7"})
    void rutEstaCorrectoTest(String rut){
        boolean resultado = usuarioService.validarRut(rut);
        Assertions.assertTrue(resultado);
    }



    @DisplayName("ingresar edad mal")
    @ParameterizedTest
    @ValueSource(ints = {-23, 3214, -123, 0 })
    void edadNoEstaCorretoTest(int edad){
        boolean resultado = usuarioService.validarEdad(edad);
        Assertions.assertFalse(resultado);
    }

    @DisplayName("ingresar edad correcta")
    @ParameterizedTest
    @ValueSource(ints = {10 , 23, 31  , 21})
    void edadEstaCorretaTest(int edad){
        boolean resultado = usuarioService.validarEdad(edad);
        Assertions.assertTrue(resultado);
    }

    @DisplayName("ingresar numero mal")
    @ParameterizedTest
    @ValueSource(ints = {132123, 12345, -12341, 99999999 })
    void numeroNoEstaCorrectoTest(int numero){
        boolean resultado = usuarioService.validarNumeroTelefonico(numero);
        Assertions.assertFalse(resultado);
    }

    @DisplayName("ingresar numero bien")
    @ParameterizedTest
    @ValueSource(ints = {992939194 , 977227676, 924534121 , 943521697})
    void numeroEstaCorrectoTest(int numero){
        boolean resultado = usuarioService.validarNumeroTelefonico(numero);
        Assertions.assertTrue(resultado);
    }

}
