package com.ufroISOFT.tareaTDD.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Usuario {
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String rut;
    private int numeroTelefonico;
    private int edad;
}
