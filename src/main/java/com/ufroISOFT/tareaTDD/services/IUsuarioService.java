package com.ufroISOFT.tareaTDD.services;

public interface IUsuarioService {
    boolean validarNombre(String nombre);

    boolean validarRut(String rut);

    boolean validarEdad(int edad);

    boolean validarNumeroTelefonico(int numero);
}
