package com.ufroISOFT.tareaTDD.services;

import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements IUsuarioService {
    @Override
    public boolean validarNombre(String nombre) {
        String patron = ".*\\d.*";
        return !nombre.matches(patron);
    }

    @Override
    public boolean validarRut(String rut) {
        rut = rut.toUpperCase().replace(".", "").replace("-", "");

        int rutNumerico;
        try {
            rutNumerico = Integer.parseInt(rut.substring(0, rut.length() - 1));
        } catch (NumberFormatException e) {
            return false;
        }

        char dv = rut.charAt(rut.length() - 1);
        int m = 0, s = 1;
        for (; rutNumerico != 0; rutNumerico /= 10) {
            s = (s + rutNumerico % 10 * (9 - m++ % 6)) % 11;
        }
        char dvCalculado = (char) (s != 0 ? s + 47 : 75);

        return dv == dvCalculado;
    }

    @Override
    public boolean validarEdad(int edad) {
        if (edad <= 0) return false;
        return edad <= 130;
    }

    @Override
    public boolean validarNumeroTelefonico(int numero) {
        if(numero < 0) return false;
        String numeroString = String.valueOf(numero);
        String patron = "^(\\+?56)?(0?9)(?!\\1{8})\\d{8}$";
        return numeroString.matches(patron);

    }
}
