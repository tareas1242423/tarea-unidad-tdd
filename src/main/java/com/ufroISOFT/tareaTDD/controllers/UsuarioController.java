package com.ufroISOFT.tareaTDD.controllers;


import com.ufroISOFT.tareaTDD.models.Usuario;
import com.ufroISOFT.tareaTDD.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UsuarioController {

    @Autowired
    private IUsuarioService usuarioService;

    @PostMapping(value = "/crear")
    public ResponseEntity<?> recibirUsuario( @RequestBody Usuario usuario){
        if(!usuarioService.validarNombre(usuario.getNombre())){
            return ResponseEntity.badRequest().body("Nombre no es valido, tiene numeros");
        }
        else if (!usuarioService.validarNombre(usuario.getApellidoPaterno())) return ResponseEntity.badRequest().body("Apellido paterno no es valido, tiene numeros");
        else if (!usuarioService.validarNombre(usuario.getApellidoMaterno())) return ResponseEntity.badRequest().body("Apellido materno no es valido, tiene numeros");
        else if (!usuarioService.validarEdad(usuario.getEdad())) {
            return ResponseEntity.badRequest().body("La edad del usuario no es valida");
        } else if (!usuarioService.validarNumeroTelefonico(usuario.getNumeroTelefonico())) {
            return ResponseEntity.badRequest().body("El numero no es valido");
        }
        else if (!usuarioService.validarRut(usuario.getRut())) return ResponseEntity.badRequest().body("El rut no es valido");

        return  ResponseEntity.status(HttpStatus.CREATED).body("Usuario recibido correctamente");
    }

}
